---
name: Component name
vueComponents: 
  - Related vue component name

#  RELATED VUE COMPONENTS should be added when available
#  Remove this section if there are no related vue components at this time. Please list them in alphabetical order.

related:
  - Related pattern name

#  RELATED PATTERNS should be similar in usage/type of pattern
#  e.g. tooltips, popover, and modals are all similar constructs used for different purposes
#  Remove this section if there are no related patterns at this time. Please list them in alphabetical order. 
---

Component overview. A brief explanation of what the component is.

## Usage

A summary of when and how the component is used.

<!-- 
  EXAMPLE TABLE, this can be used to highlight Do's & Don'ts or specific rule sets
  DO NOT add static images to any page at this time.
-->

|Component type|Purpose|
|--- |--- |
|Primary button|The primary call to action on the page.|
|Secondary button|Secondary actions on the page.|

### Sub section

<!-- 
  SUB SECTIONS, use these to highlight component specific rules. You can add as many sub-sections as needed, use your best judgement 
  e.g. truncation rules
-->

A summary of a specific usage guideline.

Todo: Add live component block with code example

## Demo

<!-- 
  DEMO, keep this section for all patterns, the code block demo will be added at a later date
-->

Todo: Add live component block with code example

## Design specifications

<!-- 
  DESIGN SPECIFICATIONS, add a link here to the component-specific sketch-measure preview.
  All design specifications should live in the design repo under 'hosted/design-gitlab-specs/COMPONENTNAME-spec-previews'
  *** If there are max-width, min-width, or other specs that should be known about this component, 
  please add it in Sketch Measur via a note.
  This link must ALWAYS be included.
-->

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for INSERT NAME OF COMPONENT HERE](/)

## Resources

*   [A related resource used when writing this documentation](/)
